<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class FinDate {

    public function convert_findate($paivays) {        

        return date("d.m.Y G.i",strtotime($paivays));
    }
    
    public function revert_findate($paivays) {
        $temp = strtotime($paivays);
        return date('Y-m-d H:i:s',$temp);        
    }
}

